import cv2 as cv
import numpy as np

'''
 # function to detect lines on road
 # takes a frame of a video and returns lines for that frame
'''


def detect_lines(frame):
    # convert original frame to grayscale
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    # detect edges on grayscale frame
    canny = cv.Canny(gray, 55, 110)

    # get region / mask in shape of triangle
    # region is excluding car hood and including road ( depth - triangle )
    height, width = canny.shape
    triangle = np.array([
        [(0, height - 100), (width // 2, height // 2 + 100), (width, height - 100)]
    ])

    # create mask with only region part of frame
    mask = np.zeros_like(canny)
    mask = cv.fillPoly(mask, triangle, 255)
    mask = cv.bitwise_and(canny, mask)

    # detect straight lines in mask
    lines = cv.HoughLinesP(mask, 1, np.pi / 180, 35, lines=None, minLineLength=15, maxLineGap=20)

    # return lines
    return lines


'''
 # function to draw lines on road
 # takes a frame of a video and lines, draws lines on frame
'''


def draw_lines(frame, lines):
    if lines is not None:
        # get points and slope for each line
        for line in lines:
            x1, y1, x2, y2 = line[0]
            # calculate slope
            if x2 - x1 != 0:
                slope = (y2 - y1) / (x2 - x1)
            else:
                slope = 0
            # if line is not vertical draw lines on original frame
            if abs(slope) > 0.5:  # around 30 degrees
                cv.line(frame, (x1, y1), (x2, y2), (255, 0, 0), 3)